package com.example.isis.listaspostres;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import Model.ListasPeliculasMod;
import adapter.AdapterPelis;

public class MainActivity extends AppCompatActivity {

    private ListView listaPostres;
    private AdapterPelis adapter;
    private List<ListasPeliculasMod> data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        data = new ArrayList<ListasPeliculasMod>();
        ListasPeliculasMod pelicula = new ListasPeliculasMod();
        pelicula.setNombre("El Rey Del Once");
        pelicula.setImg("http://www.diariodecultura.com.ar/cm/wp-content/uploads/2016/02/once-0.jpg");
        pelicula.setDescripcion("Ariel cree haber dejado atrás su pasado, distanciado de su progenitor, tras construir una nueva y exitosa vida como economista en Nueva York. Llamado por su padre, cuya misión en la vida es dirigir una fundación judía de ayuda Y beneficencia en el barrio del Once, vuelve a Buenos Aires. Allí conoce a Eva, una mujer muda e intrigante que trabaja en la fundación. Así Ariel regresa al Once, el barrio judío de su niñez. Un reencuentro con la tradición que dio origen al distanciamiento, y el enfrentarse a la paradoja de un hombre que ayuda a todo el mundo, pero es incapaz de hacerlo con su hijo Ariel.");
        pelicula.setFecha("12/04/2016");
        data.add(pelicula);

        ListasPeliculasMod pelicula2 = new ListasPeliculasMod();
        pelicula2.setNombre("Magi");
        pelicula2.setImg("http://www.morbidofest.com/wp-content/uploads/2014/11/Sin-t%C3%ADtulo.jpg?537e30");
        pelicula2.setDescripcion("Una mujer estadounidense se traslada a Estambul para trabajar como profesora de Inglés, donde además vive su hermana, que ejerce de periodista. Pero pronto se dará cuenta de que algo extraño ocurre con el nuevo bebé que espera su hermana.");
        pelicula2.setFecha("12/04/2016");
        data.add(pelicula2);


        ListasPeliculasMod pelicula3 = new ListasPeliculasMod();
        pelicula3.setNombre("Sangre De Mi Sangre");
        pelicula3.setImg("https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcQUxeZDgJ7YUE5RfjVUvHJxg0rmIrevc9VIcNPIL2Q7U27vW2ab");
        pelicula3.setDescripcion("Lydia, una joven de 16 años, es acusada de haber robado una fortuna a un cartel, pero en realidad es una trampa fraguada por su novio traficante. La chica tiene que escapar con el único aliado que tiene en el mundo: su padre, John Link (Mel Gibson), un eterno fracasado, antiguo motero rebelde y ex presidiario, que se verá en la obligación de vincularse nuevamente con un pasado del que huía para poder salvarla a ella.");
        pelicula3.setFecha("07/03/2016");
        data.add(pelicula3);


        listaPostres = (ListView) findViewById(R.id.lista_postres);
        adapter = new AdapterPelis(this,data);
        listaPostres.setAdapter(adapter);
        listaPostres.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(MainActivity.this,"Seleccion: "+data.get(position).getNombre(),Toast.LENGTH_SHORT).show();
            }
        });
    }
}
