package adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.isis.listaspostres.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import Model.ListasPeliculasMod;

/**
 * Created by julia on 12/09/2016.
 */
public class AdapterPelis extends BaseAdapter {

    Context context;
    List<ListasPeliculasMod> data;
    public AdapterPelis(Context context, List<ListasPeliculasMod> data){
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if(v==null){
            v = View.inflate(context, R.layout.ly_items_lista_pelis,null);
        }
        TextView tv_titulo = (TextView) v.findViewById(R.id.titulo);
        ImageView img = (ImageView) v.findViewById(R.id.img);
        TextView tv_descripcion = (TextView) v.findViewById(R.id.descripcion);
        TextView tv_fecha = (TextView) v.findViewById(R.id.fecha);

        ListasPeliculasMod pelicula = (ListasPeliculasMod) getItem(position);
        tv_titulo.setText(pelicula.getNombre());
        Picasso.with(context).load(pelicula.getImg()).into(img);
        tv_descripcion.setText(pelicula.getDescripcion());
        tv_fecha.setText(pelicula.getFecha());
        v.setBackgroundColor(v.getResources().getColor(R.color.colorPrimaryDark));
        if(position % 2 == 0){
            //v.setBackgroundColor(v.getResources().getColor(R.color.colorAccent));
        }else{
            //v.setBackgroundColor(v.getResources().getColor(R.color.colorPrimary));
        }
        return v;
    }
}
